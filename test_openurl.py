from selenium import webdriver
import pytest

driver = webdriver.Chrome("E:\IYA\Ins\chromedriver_win32\chromedriver.exe")
driver.minimize_window()

Alamat = [
    ("https://www.google.com", "Google"),
    ("https://www.facebook.com", "Facebook - Masuk atau Daftar")
]

@pytest.mark.parametrize('address, result', Alamat)

def test_bukaUrl(address, result):
    driver.get(address)
    Title = driver.title
    assert Title == result