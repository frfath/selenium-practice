from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

driver = webdriver.Chrome("E:\IYA\Ins\chromedriver_win32\chromedriver.exe")

driver.get("https://rahulshettyacademy.com/AutomationPractice/")
driver.implicitly_wait(10)
'''
def test_checkbox():
    # Get checkbox element
    checkbox = driver.find_element(By.ID, "checkBoxOption1")
    # Click checkbox
    checkbox.click()
    # Check if checkbox is selected
    isSelected = checkbox.is_selected()
    assert isSelected == True
    # Click checkbox
    checkbox.click()
    # Check if checkbox is selected
    isSelected = checkbox.is_selected()
    assert isSelected == False

def test_radiobutton():
    # Get radio button element
    radioButton = driver.find_element(By.XPATH, '//*[@id="radio-btn-example"]/fieldset/label[1]/input')
    # Click radio button
    radioButton.click()
    # Check if radio button is selected
    isSelected = radioButton.is_selected()
    assert isSelected == True
    # Get radio button2 element
    radioButton2 = driver.find_element(By.XPATH, '//*[@id="radio-btn-example"]/fieldset/label[2]/input')
    # Click radio button2
    radioButton2.click()
    # Check if radio button1 is not selected and radio button2 is selected
    isSelected = radioButton.is_selected()
    isSelected2 = radioButton2.is_selected()
    assert isSelected == False
    assert isSelected2 == True

def test_dropdown():
    # Select dropdown
    dropdown = Select(driver.find_element(By.TAG_NAME, 'select'))
    # Get all options
    options = []
    for opt in dropdown.options:
        options.append(opt.text)
    # Check all options
    assert options == ["Select", "Option1", "Option2", "Option3"]
    # Select option in first index 
    dropdown.select_by_index(1)
    # Get selected option text
    selectedOption = dropdown.first_selected_option.text
    # Check the selected option
    assert selectedOption == "Option1"

def test_autocomplete():
    # Select box
    autocomplete = driver.find_element(By.ID, 'autocomplete')
    # Type text in the box
    autocomplete.send_keys('Ind')
    # Get all suggestions
    suggestionList, suggestions = []
    suggestionList = driver.find_element(By.CSS_SELECTOR, 'li[class="ui-menu-item"]')
    for suggestion in suggestionList:
        suggestions.append(suggestion.text)
    # Check all suggestions
    # for suggestion in suggestions:
    #    assert 'Ind' in suggestion.text
    assert all('Ind' in suggestion for suggestion in suggestions)

def test_openWindow():
    # Store the ID of the original window
    original_window = driver.current_window_handle

    # Check we don't have other windows open already
    assert len(driver.window_handles) == 1

    # Click the link which opens in a new window
    driver.find_element(By.ID, 'openwindow').click()
    
    # Check if the other windows open already
    assert len(driver.window_handles) == 2
'''
def test_openTab():
    # Store the ID of the original tab
    original_tab = driver.current_window_handle

    # Check we don't have other tab open already
    assert len(driver.window_handles) == 1

    # Click the link which opens in a new tab
    driver.find_element(By.ID, 'opentab').click()
    
    # Check if the other tab open already
    # assert len(driver.window_handles) == 2
    for window_handle in driver.window_handles:
        if window_handle != original_tab:
            driver.switch_to.window(window_handle)
            assert driver.title == "Rahul Shetty Academy"
        else:
            driver.switch_to.window(window_handle)
            assert driver.title == "Practice Page"
        