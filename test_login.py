from selenium import webdriver
import pytest

driver = webdriver.Chrome("E:\IYA\Ins\chromedriver_win32\chromedriver.exe")
driver.maximize_window()
driver.implicitly_wait(10)

Credential = [
    ("selprac", "12345"),
    ("sel", "PoijklmnB0!"),
    ("sel", "12345")
]

@pytest.mark.parametrize('username, password', Credential)

def test_login(username, password):
    driver.get("https://demoqa.com/login")
    driver.find_element_by_id("userName").send_keys(username)
    driver.find_element_by_id("password").send_keys(password)
    driver.find_element_by_id("login").click()

    invalidText = driver.find_element_by_id("name").text
    
    assert invalidText == "Invalid username or password!"